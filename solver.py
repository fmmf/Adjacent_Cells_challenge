import json

def load_data(file_name):
    """ load matrix from a json file """
    with open(file_name, 'r') as outfile:
        data = json.load(outfile)
    outfile.close()
    return data

def get_points(list):
    """ get list with all coordinates with value 1 """
    for i in range(len(list)):
        for j in range(len(list[0])):
            if list[i][j] == 1:
                lista.append([i,j])

def get_group(coord = None):
    """ get list with coordinates of all points from a group """
    if coord is None:
        coord = [lista[0][0], lista[0][1]]
    i = coord[0]
    j = coord[1]
    if [i,j] not in result and [i,j]  in lista:
        result.append([i,j])
    if coord in lista:
        lista.remove(coord)
    for elem in ([i-1, j], [i+1, j], [i, j-1], [i, j+1]):
        if elem in lista:
            result.append(elem)
            lista.remove(elem)
            get_group(elem)

lista = []
final = []

list = load_data("100x100.json")
get_points(list)

while len(lista)> 0:
    result = []
    get_group()
    if len(result)>1:
     final.append(result)

print (final)
